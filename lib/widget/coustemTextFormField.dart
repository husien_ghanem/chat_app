import 'package:flutter/material.dart';

import '../resources/color_manager.dart';
import '../resources/strings_manager.dart';

class CoustemTextFormField extends StatelessWidget {
  final Function(String value)? function;
  bool obscureText = false;
  final String? hintText;
  final TextInputType? textInputType;
  CoustemTextFormField(
      {required this.function,
      required this.hintText,
      required this.obscureText,
      this.textInputType});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: textInputType,
      validator: (value) {
        if (value?.length == null || value!.length < 5) {
          return AppStrings.noLessFiveLetters;
        }
      },
      onChanged: function,
      style: TextStyle(color: ColorManager.blackToWrite),
      obscureText: obscureText,
      decoration: InputDecoration(
          fillColor: Colors.grey.shade100,
          filled: true,
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(
              color: ColorManager.primaryGray,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(color: ColorManager.brightBlue, width: 2),
          ),
          hintText: hintText,
          hintStyle: TextStyle(
            color: ColorManager.blackToWrite,
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          )),
    );
  }
}
