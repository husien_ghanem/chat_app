import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';

import '../resources/assets_manager.dart';
import '../resources/color_manager.dart';
import '../resources/strings_manager.dart';
import '../widget/coustemTextFormField.dart';

class MyLogin extends StatefulWidget {
  const MyLogin({Key? key}) : super(key: key);

  @override
  _MyLoginState createState() => _MyLoginState();
}

class _MyLoginState extends State<MyLogin> {
  String? emailAddress;
  bool isProgressing = false;
  String? passWord;
  GlobalKey<FormState> formKey = GlobalKey();
  @override
  Widget build(BuildContext context) {
    return ModalProgressHUD(
      inAsyncCall: isProgressing,
      child: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage(ImageAssets.login), fit: BoxFit.cover),
        ),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: Stack(
            children: [
              Container(),
              Container(
                padding: const EdgeInsets.only(left: 35, top: 130),
                child: Text(
                  AppStrings.welcome,
                  style: TextStyle(color: ColorManager.white, fontSize: 33),
                ),
              ),
              SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: const EdgeInsets.only(left: 35, right: 35),
                        child: Form(
                          key: formKey,
                          child: Column(
                            children: [
                              CoustemTextFormField(
                                  function: (value) {
                                    emailAddress = value;
                                  },
                                  hintText: "Email",
                                  obscureText: false),
                              const SizedBox(
                                height: 30,
                              ),
                              CoustemTextFormField(
                                  function: (value) {
                                    passWord = value;
                                  },
                                  hintText: "Password",
                                  obscureText: false),
                              const SizedBox(
                                height: 40,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  const Text(
                                    'Sign in',
                                    style: TextStyle(
                                        fontSize: 27,
                                        fontWeight: FontWeight.w700),
                                  ),
                                  CircleAvatar(
                                    radius: 30,
                                    backgroundColor: ColorManager.primaryGray,
                                    child: IconButton(
                                        color: ColorManager.white,
                                        onPressed: () async {
                                          if (formKey.currentState!
                                              .validate()) {
                                            setState(() {
                                              isProgressing = true;
                                            });
                                            try {
                                              await signInUser(context);
                                              Navigator.pushReplacementNamed(context, "mainPageChat");
                                            } on FirebaseAuthException catch (e) {
                                              setState(() {
                                                isProgressing = false;
                                              });
                                              firebaseExcep(e, context);
                                            } catch (e) {
                                              ScaffoldMessenger.of(context)
                                                  .showSnackBar(SnackBar(
                                                      content: Text(
                                                "$e",
                                                maxLines: 2,
                                              )));
                                            }
                                          }
                                        },
                                        icon: const Icon(
                                          Icons.arrow_forward,
                                        )),
                                  )
                                ],
                              ),
                              const SizedBox(
                                height: 40,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  TextButton(
                                    onPressed: () {
                                      Navigator.pushNamed(context, 'register');
                                    },
                                    style: const ButtonStyle(),
                                    child: Text(
                                      'Sign Up',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          decoration: TextDecoration.underline,
                                          color: ColorManager.primaryGray,
                                          fontSize: 18),
                                    ),
                                  ),
                                  TextButton(
                                      onPressed: () {},
                                      child: Text(
                                        'Forgot Password',
                                        style: TextStyle(
                                          decoration: TextDecoration.underline,
                                          color: ColorManager.primaryGray,
                                          fontSize: 18,
                                        ),
                                      )),
                                ],
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void firebaseExcep(FirebaseAuthException e, BuildContext context) {
    if (e.code == 'user-not-found') {
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('user-not-found for this Email')));
    } else if (e.code == 'wrong-password') {
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('wrong-password for this email')));
    }
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(
      "Wrong Value $e",
      maxLines: 3,
      overflow: TextOverflow.ellipsis,
    )));
  }

  Future<void> signInUser(BuildContext context) async {
    await FirebaseAuth.instance.signInWithEmailAndPassword(
      email: emailAddress!,
      password: passWord!,
    );

    setState(() {
      isProgressing = false;
    });

    // ignore: use_build_context_synchronously
    Navigator.pushNamedAndRemoveUntil(
      context,
      'ChatPages',
      (Route<dynamic> route) => false,
    );
  }
}
