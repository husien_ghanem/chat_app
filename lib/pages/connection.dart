import 'package:firebase_app/pages/login.dart';
import 'package:flutter/material.dart';
import 'package:flutter_offline/flutter_offline.dart';

import '../resources/assets_manager.dart';
import '../resources/color_manager.dart';
import '../resources/strings_manager.dart';

class ConnectClass extends StatefulWidget {
  const ConnectClass({Key? key}) : super(key: key);

  @override
  State<ConnectClass> createState() => _ConnectClassState();
}

class _ConnectClassState extends State<ConnectClass> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: OfflineBuilder(
        connectivityBuilder: (
          BuildContext context,
          ConnectivityResult connectivity,
          Widget child,
        ) {
          final bool connected = connectivity != ConnectivityResult.none;

          if (connected) {
            return const MyLogin();
          } else {
            return buildNoInternetWidget();
          }
        },
        child: showLoadingIndicator(),
      ),
    );
  }
}

Widget showLoadingIndicator() {
  return const Center(
    child: CircularProgressIndicator(
      color: Colors.blue,
    ),
  );
}

Widget buildNoInternetWidget() {
  return Center(
    child: Container(
      color: ColorManager.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const SizedBox(
            height: 20,
          ),
          const Text(
            AppStrings.noEnternet,
            style: TextStyle(
              fontSize: 22,
              color: Colors.grey,
            ),
          ),
          Image.asset(ImageAssets.noInternet)
        ],
      ),
    ),
  );
}
