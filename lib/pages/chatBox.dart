import 'package:flutter/material.dart';
import 'package:grouped_list/grouped_list.dart';
import 'package:intl/intl.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../model/messages.dart';
import '../resources/color_manager.dart';

class MainPageChat extends StatefulWidget {
  const MainPageChat({Key? key}) : super(key: key);

  @override
  State<MainPageChat> createState() => _MainPageChatState();
}

class _MainPageChatState extends State<MainPageChat> {
  CollectionReference inMessagesFire =
      FirebaseFirestore.instance.collection('messages');
  TextEditingController controller = TextEditingController();
  List<Messages> messageList = [];
  bool isLoading = true;

  Messages? myMessage;
  Future<void> _getMessages() async {
    final remoteMessages =
        await inMessagesFire.orderBy('date', descending: false).get();
    messageList.clear();
    for (int i = 0; i < remoteMessages.docs.length; i++) {
      messageList.add(Messages.fromJson(remoteMessages.docs[i]));
    }
    setState(() {
      isLoading = false;
    });
  }

  Future _sendMessage() async {
    if (controller.text.trim().isEmpty) return;
    debugPrint("clicked");
    await inMessagesFire.add({
      "text": controller.text,
      "date": DateTime.now(),
      "isSentByMe": true
    }).then((_) async {
      controller.clear();
      await _getMessages();
      setState(() {});
    });
  }

  @override
  void initState() {
    _getMessages();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        backgroundColor: ColorManager.brightBlue,
        title: const Center(child: Text('Chat app')),
      ),
      body: isLoading
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : Column(
              children: [
                _buildMessageWidget(),
                Row(
                  children: [_buildSendTextField(context), _buildSendIcon()],
                ),
              ],
            ),
    );
  }

  Padding _buildSendIcon() {
    return Padding(
        padding: const EdgeInsets.all(4.0),
        child: InkWell(
            onTap: () => _sendMessage(),
            child: Container(
              padding: const EdgeInsets.all(13),
              decoration: BoxDecoration(
                  color: Colors.blue, borderRadius: BorderRadius.circular(50)),
              child: const Icon(
                Icons.send,
                color: Colors.white,
              ),
            )));
  }

  Expanded _buildSendTextField(BuildContext context) {
    return Expanded(
      child: TextField(
        controller: controller,
        decoration: InputDecoration(
            filled: true,
            fillColor: Colors.grey.shade300,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(35),
            ),
            contentPadding: const EdgeInsets.all(12),
            hintText: 'Type your message here...'),
      ),
    );
  }

  Expanded _buildMessageWidget() {
    return Expanded(
        child: GroupedListView<Messages, DateTime>(
      physics: const BouncingScrollPhysics(),
      reverse: true,
      order: GroupedListOrder.DESC,
      useStickyGroupSeparators: true,
      floatingHeader: true,
      padding: const EdgeInsets.all(8),
      elements: messageList,
      groupBy: (instanceOfMessages) => DateTime(
        instanceOfMessages.date.year,
        instanceOfMessages.date.month,
        instanceOfMessages.date.day,
      ),
      groupHeaderBuilder: (Messages message) => SizedBox(
        height: 40,
        child: Center(
            child: Card(
          color: ColorManager.brightBlue,
          child: Padding(
            padding: const EdgeInsets.all(8),
            child: Text(
              DateFormat.yMMMEd().format(message.date),
              style: TextStyle(
                  color: ColorManager.primaryGray, fontWeight: FontWeight.bold),
            ),
          ),
        )),
      ),
      itemBuilder: (context, Messages message) => message.isSendByMe
          ? Padding(
              padding: const EdgeInsets.all(8.0),
              child: Align(
                alignment: Alignment.bottomRight,
                child: Container(
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: ColorManager.primaryGray,
                          spreadRadius: 1,
                          blurRadius: 4,
                          offset:
                              const Offset(0, 2), // changes position of shadow
                        ),
                      ],
                      borderRadius: const BorderRadius.only(
                          bottomLeft: Radius.circular(15),
                          topLeft: Radius.circular(15),
                          topRight: Radius.circular(20)),
                      color: ColorManager.primaryGray),
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Text(
                      message.text,
                      style: TextStyle(color: ColorManager.white),
                    ),
                  ),
                ),
              ),
            )
          : Padding(
              padding: const EdgeInsets.all(8.0),
              child: Align(
                alignment: Alignment.bottomLeft,
                child: Container(
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: ColorManager.primaryGray,
                          spreadRadius: 1,
                          blurRadius: 4,
                          offset:
                              const Offset(0, 2), // changes position of shadow
                        ),
                      ],
                      borderRadius: const BorderRadius.only(
                          bottomRight: Radius.circular(15),
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(15)),
                      color: ColorManager.primaryGray),
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Text(
                      message.text,
                      style: TextStyle(color: ColorManager.white),
                    ),
                  ),
                ),
              ),
            ),
    ));
  }
}
