import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';

import '../resources/assets_manager.dart';
import '../resources/color_manager.dart';
import '../resources/strings_manager.dart';
import '../widget/coustemTextFormField.dart';

class MyRegister extends StatefulWidget {
  const MyRegister({Key? key}) : super(key: key);

  @override
  _MyRegisterState createState() => _MyRegisterState();
}

class _MyRegisterState extends State<MyRegister> {
  String? emailAddress;
  bool isProgressing = false;
  String? passWord;
  GlobalKey<FormState> formKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return ModalProgressHUD(
      inAsyncCall: isProgressing,
      child: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage(ImageAssets.register), fit: BoxFit.cover),
        ),
        child: Scaffold(
          backgroundColor: ColorManager.black,
          appBar: AppBar(
            backgroundColor: ColorManager.black,
            elevation: 0,
          ),
          body: Stack(
            children: [
              Container(
                padding: const EdgeInsets.only(left: 35, top: 30),
                child: const Text(
                  AppStrings.createAccount,
                  style: TextStyle(color: Colors.white, fontSize: 33),
                ),
              ),
              SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.28),
                  child: Form(
                    key: formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: const EdgeInsets.only(left: 35, right: 35),
                          child: Column(
                            children: [
                              const SizedBox(
                                height: 30,
                              ),
                              CoustemTextFormField(
                                  textInputType: TextInputType.emailAddress,
                                  hintText: 'Email',
                                  obscureText: false,
                                  function: (value) {
                                    emailAddress = value;
                                  }),
                              const SizedBox(
                                height: 30,
                              ),
                              CoustemTextFormField(
                                  hintText: 'Password',
                                  obscureText: true,
                                  function: (value) {
                                    passWord = value;
                                  }),
                              const SizedBox(
                                height: 40,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Sign Up',
                                    style: TextStyle(
                                        color: ColorManager.white,
                                        fontSize: 27,
                                        fontWeight: FontWeight.w700),
                                  ),
                                  CircleAvatar(
                                    radius: 30,
                                    backgroundColor: ColorManager.primaryGray,
                                    child: IconButton(
                                        color: ColorManager.white,
                                        onPressed: () async {
                                       
                                          if (formKey.currentState!
                                              .validate()) {
                                            setState(() {
                                              isProgressing = true;
                                            });
                                            try {
                                              await createUser(context);
                          Navigator.pushReplacementNamed(context, "mainPageChat");                     
                                            } on FirebaseAuthException catch (e) {
                                              setState(() {
                                                isProgressing = false;
                                              });
                                              firebaseExcep(e, context);
                                            } catch (e) {
                                              ScaffoldMessenger.of(context)
                                                  .showSnackBar(SnackBar(
                                                      content: Text(
                                                "$e",
                                                maxLines: 2,
                                              )));
                                            }
                                          }
                                        },
                                        icon: const Icon(
                                          Icons.arrow_forward,
                                        )),
                                  )
                                ],
                              ),
                              const SizedBox(
                                height: 40,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  TextButton(
                                    onPressed: () {
                                      Navigator.pushNamed(context, 'login');
                                      // Navigator.of(context).push(_createRoute());
                                    },
                                    style: const ButtonStyle(),
                                    child: const Text(
                                      'Sign In',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          decoration: TextDecoration.underline,
                                          color: Colors.white,
                                          fontSize: 18),
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void firebaseExcep(FirebaseAuthException e, BuildContext context) {
    if (e.code == 'weak-password') {
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('The password provided is too weak')));
    } else if (e.code == 'email-already-in-use') {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text('The account already exists for that email')));
    }
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(
      "Wrong Value $e",
      maxLines: 3,
      overflow: TextOverflow.ellipsis,
    )));
  }

  Future<void> createUser(BuildContext context) async {
    await FirebaseAuth.instance.createUserWithEmailAndPassword(
      email: emailAddress!,
      password: passWord!,
    );

    setState(() {
      isProgressing = false;
    });

    // ignore: use_build_context_synchronously
    Navigator.pushNamedAndRemoveUntil(
      context,
      'ChatPages',
      (Route<dynamic> route) => false,
    );
  }
}
