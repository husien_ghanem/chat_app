class ImageAssets {
  static const String noInternet = "assets/images/no_internet.png";
  static const String login = "assets/images/login.png";
  static const String register = "assets/images/register.png";
}
