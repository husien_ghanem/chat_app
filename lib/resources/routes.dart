import 'package:firebase_app/pages/connection.dart';

import 'package:flutter/material.dart';


import '../pages/chatBox.dart';
import '../pages/login.dart';
import '../pages/register.dart';

class RouteGenerate {
  static Route<dynamic>? generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => const ConnectClass());
      case 'login':
        return MaterialPageRoute(builder: (_) => const MyLogin());
      case 'register':
        return MaterialPageRoute(builder: (_) => const MyRegister());
      case 'mainPageChat':
        return MaterialPageRoute(builder: (_) => const MainPageChat());
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: const Text("Error"),
        ),
        body: const Center(
          child: Text("ERROR"),
        ),
      );
    });
  }
}
