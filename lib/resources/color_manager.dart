import 'package:flutter/material.dart';

class ColorManager {
  static Color primaryGray = const Color(0xff4c505b);
  static Color brightBlue = const Color.fromARGB(255, 95, 183, 255);
  static Color orange = const Color.fromARGB(255, 255, 149, 28);
  static Color white = const Color(0xffFFFFFF);
  static Color blackToWrite = const Color.fromARGB(255, 26, 27, 34);
  static Color black = Colors.transparent;
}
