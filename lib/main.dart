import 'package:firebase_app/pages/login.dart';
import 'package:firebase_app/resources/routes.dart';
import 'package:firebase_app/pages/chatBox.dart';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

import 'firebase_options.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  runApp(const MaterialApp(
    debugShowCheckedModeBanner: false,
    onGenerateRoute: RouteGenerate.generateRoute,
    initialRoute: '/',

  ));
}
