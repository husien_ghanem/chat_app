class Messages {
  final String text;
  final bool isSendByMe;
  final DateTime date;

  Messages({required this.text, required this.isSendByMe, required this.date});
  factory Messages.fromJson(json) {
    return Messages(
        text: json["text"],
        isSendByMe: json["isSentByMe"],
        date: json["date"].toDate());
  }
}
